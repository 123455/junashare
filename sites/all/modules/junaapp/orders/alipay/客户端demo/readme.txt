一、免责声明：
Demo仅供参考，实际开发中需要结合具体业务场景修改使用。

二、Demo运行环境
1）Android：支持2.3及以上的系统版本运行。
2）iOS：iOS 6.0以上(包含iOS 6.0)

三、说明：
如果您已经集成百川sdk，在集成支付宝SDK过程中发现有UTDID冲突。
请使用 "AlipaySDK_No_UTDID(适用于集成了百川sdk，出现UTDID冲突).zip" 压缩包。
此SDK删除了UTDID的依赖。